from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import Pergunta, Alternativa
#from django.template import loader
from django.http import Http404
from django.views import generic



class IndexView(generic.ListView):
    model = Pergunta
    template_name = 'enquetes/index.html'
    context_object_name = 'lista_enquetes'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_pub')


class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/detalhes.html'

class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'


def votacao(request, enquete_id):
    try:
        pergunta = get_object_or_404(Pergunta, pk = enquete_id)
        alt_id = request.POST['alt']
        alternativa = pergunta.alternativa_set.get(pk=alt_id)

    except(KeyError, Alternativa.DoesNotExist):
        return render(request, 'enquetes/detalhes.html' , {'pergunta': pergunta,
        'error': 'Selecione uma alternativa válida'})
    else:
        alternativa.quant_votos += 1
        alternativa.save()
        return HttpResponseRedirect(reverse(
                'enquetes:resultado', args=(pergunta.id,)
            ))



"""
#### HISTÓRICO DAS VERSÕES ####
###############################

## VIEW INDEX ##
################
----versão 1
    resultado = ',<br/> '.join([p.texto for p in lista_enquetes])
    return HttpResponse(resultado)
----versão 2
    template = loader.get_template('enquetes/index.html')
    return HttpResponse(template.render(contexto, request))
----versão 3
def index(request):
    lista_enquetes = Pergunta.objects.order_by('-data_pub')[:10]
    contexto = { 'lista_enquetes': lista_enquetes }
    return render(request, 'enquetes/index.html', contexto)
----vesão 4
class IndexView(generic.ListView):
    template_name = 'enquetes/index.html'
    context_object_name = 'lista_enquetes'
    def get_queryset(self):
        return Pergunta.objects.order_by('-data_pub')

## VIEW DETALHES ##
###################
----versão 1
    retorno = "<h2>DETALHES da enquete de identificador = %s</h2>"
    return HttpResponse(retorno % enquete_id)
----versão 2
    try:
        pergunta = Pergunta.objects.get(pk = enquete_id)
    except Pergunta.DoesNotExist:
        raise Http404("Identificador de enquete inválido!")
----versão 3
def detalhes(request, enquete_id):
    pergunta = get_object_or_404(Pergunta, pk = enquete_id)
    return render(
        request, 'enquetes/detalhes.html', {'pergunta': pergunta}
    )
----versão 4
class DetalhesView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/detalhes.html'
## VIEW RESULTADO ##
####################
----versão 1
def resultado(request, enquete_id):
    pergunta = get_object_or_404(Pergunta, pk = enquete_id)
    return render(
        request, 'enquetes/resultado.html', {'pergunta': pergunta}
    )
----versão 2
class ResultadoView(generic.DetailView):
    model = Pergunta
    template_name = 'enquetes/resultado.html'
## VIEW VOTACAO ##
##################

"""


