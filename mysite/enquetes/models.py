import datetime
from django.db import models
from django.utils import timezone

#As classes já definem uma chave primária automática 'ID'
#Neste caso é necessário definir os tipos dos atributos, pois aqui eles são bases dos atributos do banco de dados

#max_length: define uma quantidade limite de caracteres
#default: define um valor padrão para as pergutas
#on_delete: se uma pergunta for apagada todas as alternativas serão apagadas em forma de cascata

class Pergunta(models.Model):
    texto = models.CharField(max_length = 150)
    data_pub = models.DateTimeField('Data de publicação')

    def __str__(self):
        return '({}) - {}'.format(self.id, self.texto)

    def publicada_recentemente(self):
        return self.data_pub >= timezone.now() - datetime.timedelta(days=1)


class Alternativa(models.Model):
    texto = models.CharField(max_length = 80)
    quant_votos = models.IntegerField('Quantidade de votos', default = 0)
    pergunta = models.ForeignKey(Pergunta, on_delete = models.CASCADE)

    def __str__(self):
        return '({}) - {}'.format(self.id, self.texto)




#Eu posso ter uma Perguta relacionada a varias Alternativas, mas um Alternativa deve está relacionada a apenas uma Pergunta